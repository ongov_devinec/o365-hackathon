import * as Msal from 'msal';

export default class AuthService {
  constructor() {
    this.applicationConfig = {
        auth: {
            clientId: '90f81fcd-9053-478f-849e-ce35c45ec44c', //This is your client ID
            authority: "https://login.microsoftonline.com/cddc1229-ac2a-4b97-b78a-0e5cacb5865c", //This is your tenant info
            redirectUri: "http://localhost:8080"
        },
        cache: {
            cacheLocation: "localStorage",
            storeAuthStateInCookie: true
        }
    };
    this.requestObj = {
      scopes: ['user.read', 'calendars.read', 'User.ReadBasic.All', 'People.Read','profile','email','openid']
    }
    this.app = new Msal.UserAgentApplication(this.applicationConfig);
  }
  login() {
    return this.app.loginPopup(this.requestObj).then(
      idToken => {
        const user = this.app.getAccount();
        if (user) {
          return user;
        } else {
          return null;
        }
      },
      error => {
        console.log(error);
        return null;
      }
    );
  };
  logout() {
    this.app.logout();
  };
  getAccount(){
    if (this.app.getAccount()) {
            return this.app.getAccount();
        } else {
            return false;
        }
  };
  getToken() {
    return this.app.acquireTokenSilent(this.requestObj).then(
      accessToken => {
        return accessToken;
      },
      error => {
        return this.app
          .acquireTokenPopup(this.requestObj)
          .then(
            accessToken => {
              return accessToken;
            },
            err => {
              console.error(err);
            }
          );
      }
    );
  };
}